# manifest-server

![MicroBadger Size](https://img.shields.io/microbadger/image-size/robertograham/manifest-server.svg?style=flat-square) ![Docker Automated build](https://img.shields.io/docker/automated/robertograham/manifest-server.svg?style=flat-square) ![Docker Build Status](https://img.shields.io/docker/build/robertograham/manifest-server.svg?style=flat-square)

## Usage

### Run with docker-compose

Download [docker-compose.yml](https://bitbucket.org/robertograham/manifest-app/src/master/docker-compose.yml)

```bash
docker-compose up
```

### Run with docker-app

```bash
docker-app render robertograham/manifest | docker-compose -f - up
```

## Contributing

Pull requests cannot be accepted as its a university project. You may raise issues but please don't include code snippets

## Built with

* [Spring Boot](https://github.com/spring-projects/spring-boot)

* [Docker Client](https://github.com/spotify/docker-client)