package robertograham.manifest.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManifestServerApplication {

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "manifest-server");
        SpringApplication.run(ManifestServerApplication.class, args);
    }
}
