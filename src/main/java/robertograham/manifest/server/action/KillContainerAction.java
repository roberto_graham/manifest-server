package robertograham.manifest.server.action;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public final class KillContainerAction {

    private final String containerId;
    private final String killSignal;

    @JsonCreator
    public KillContainerAction(final String containerId, final String killSignal) {
        this.containerId = Objects.requireNonNull(containerId, "containerId cannot be null");
        this.killSignal = Objects.requireNonNull(killSignal, "killSignal cannot be null");
    }

    public String getContainerId() {
        return containerId;
    }

    public String getKillSignal() {
        return killSignal;
    }

    @Override
    public String toString() {
        return "KillContainerAction{" +
            "containerId='" + containerId + '\'' +
            ", killSignal='" + killSignal + '\'' +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof KillContainerAction))
            return false;
        final var killContainerAction = (KillContainerAction) object;
        return containerId.equals(killContainerAction.containerId) &&
            killSignal.equals(killContainerAction.killSignal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId, killSignal);
    }
}
