package robertograham.manifest.server.action;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public final class PauseContainerAction {

    private final String containerId;

    @JsonCreator
    public PauseContainerAction(final String containerId) {
        this.containerId = Objects.requireNonNull(containerId, "containerId cannot be null");
    }

    public String getContainerId() {
        return containerId;
    }

    @Override
    public String toString() {
        return "PauseContainerAction{" +
            "containerId='" + containerId + '\'' +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof PauseContainerAction))
            return false;
        final var pauseContainerAction = (PauseContainerAction) object;
        return containerId.equals(pauseContainerAction.containerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId);
    }
}
