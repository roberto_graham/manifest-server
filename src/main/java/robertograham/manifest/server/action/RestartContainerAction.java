package robertograham.manifest.server.action;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public final class RestartContainerAction {

    private final String containerId;
    private final int waitSeconds;

    @JsonCreator
    public RestartContainerAction(final String containerId, final Integer waitSeconds) {
        this.containerId = Objects.requireNonNull(containerId, "containerId cannot be null");
        this.waitSeconds = Objects.requireNonNull(waitSeconds, "waitSeconds cannot be null");
    }

    public String getContainerId() {
        return containerId;
    }

    public int getWaitSeconds() {
        return waitSeconds;
    }

    @Override
    public String toString() {
        return "RestartContainerAction{" +
            "containerId='" + containerId + '\'' +
            ", waitSeconds=" + waitSeconds +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof RestartContainerAction))
            return false;
        final var restartContainerAction = (RestartContainerAction) object;
        return waitSeconds == restartContainerAction.waitSeconds &&
            containerId.equals(restartContainerAction.containerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId, waitSeconds);
    }
}
