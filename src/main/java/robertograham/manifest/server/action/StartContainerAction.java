package robertograham.manifest.server.action;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public final class StartContainerAction {

    private final String containerId;

    @JsonCreator
    public StartContainerAction(final String containerId) {
        this.containerId = Objects.requireNonNull(containerId, "containerId cannot be null");
    }

    public String getContainerId() {
        return containerId;
    }

    @Override
    public String toString() {
        return "StartContainerAction{" +
            "containerId='" + containerId + '\'' +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof StartContainerAction))
            return false;
        final var startContainerAction = (StartContainerAction) object;
        return containerId.equals(startContainerAction.containerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId);
    }
}
