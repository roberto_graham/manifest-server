package robertograham.manifest.server.action;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public final class StopContainerAction {

    private final String containerId;
    private final int waitSeconds;

    @JsonCreator
    public StopContainerAction(final String containerId, final Integer waitSeconds) {
        this.containerId = Objects.requireNonNull(containerId, "containerId cannot be null");
        this.waitSeconds = Objects.requireNonNull(waitSeconds, "waitSeconds cannot be null");
    }

    public String getContainerId() {
        return containerId;
    }

    public int getWaitSeconds() {
        return waitSeconds;
    }

    @Override
    public String toString() {
        return "StopContainerAction{" +
            "containerId='" + containerId + '\'' +
            ", waitSeconds=" + waitSeconds +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof StopContainerAction))
            return false;
        final var stopContainerAction = (StopContainerAction) object;
        return waitSeconds == stopContainerAction.waitSeconds &&
            containerId.equals(stopContainerAction.containerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId, waitSeconds);
    }
}
