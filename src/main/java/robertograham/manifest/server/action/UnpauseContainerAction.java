package robertograham.manifest.server.action;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Objects;

public final class UnpauseContainerAction {

    private final String containerId;

    @JsonCreator
    public UnpauseContainerAction(final String containerId) {
        this.containerId = Objects.requireNonNull(containerId, "containerId cannot be null");
    }

    public String getContainerId() {
        return containerId;
    }

    @Override
    public String toString() {
        return "UnpauseContainerAction{" +
            "containerId='" + containerId + '\'' +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof UnpauseContainerAction))
            return false;
        final var unpauseContainerAction = (UnpauseContainerAction) object;
        return containerId.equals(unpauseContainerAction.containerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containerId);
    }
}
