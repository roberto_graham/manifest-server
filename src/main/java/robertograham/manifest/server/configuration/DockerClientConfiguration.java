package robertograham.manifest.server.configuration;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DockerClientConfiguration {

    @Bean(destroyMethod = "close")
    public DockerClient dockerClient() throws DockerCertificateException, DockerException, InterruptedException {
        final var defaultDockerClient = DefaultDockerClient.fromEnv()
            .build();
        defaultDockerClient.ping();
        return defaultDockerClient;
    }
}
