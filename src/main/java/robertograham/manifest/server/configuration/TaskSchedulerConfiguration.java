package robertograham.manifest.server.configuration;

import org.springframework.boot.task.TaskSchedulerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@Configuration
public class TaskSchedulerConfiguration {

    @Bean
    public TaskScheduler taskScheduler() {
        return new TaskSchedulerBuilder()
            .poolSize(1)
            .build();
    }
}
