package robertograham.manifest.server.configuration;

import com.spotify.docker.client.DockerClient.Signal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import robertograham.manifest.server.viewstate.ContainerViewState;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Configuration
public class ViewStateConfiguration {

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public ContainerViewState containerViewState() {
        return new ContainerViewState(
            Collections.emptyList(),
            Stream.of(Signal.values())
                .map(Signal::name)
                .collect(Collectors.toUnmodifiableSet())
        );
    }
}
