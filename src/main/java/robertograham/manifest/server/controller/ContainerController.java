package robertograham.manifest.server.controller;

import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import robertograham.manifest.server.action.*;
import robertograham.manifest.server.service.ContainerService;
import robertograham.manifest.server.viewstate.ContainerViewState;

import java.util.Objects;

@Controller
public final class ContainerController {

    private final ContainerService containerService;

    @Autowired
    public ContainerController(final ContainerService containerService) {
        this.containerService = Objects.requireNonNull(containerService, "containerService cannot be null");
    }

    @SubscribeMapping("/queue/container")
    public ContainerViewState containerViewState() {
        return containerService.viewState();
    }

    @SubscribeMapping("/queue/container/{containerId}/statistics")
    public ContainerStats containerStatistics(@DestinationVariable final String containerId) throws DockerException, InterruptedException {
        return containerService.getStatisticsByContainerId(containerId);
    }

    @MessageMapping("/container/stop")
    public void stopContainer(final StopContainerAction stopContainerAction) throws DockerException, InterruptedException {
        containerService.stopContainer(stopContainerAction);
    }

    @MessageMapping("/container/pause")
    public void pauseContainer(final PauseContainerAction pauseContainerAction) throws DockerException, InterruptedException {
        containerService.pauseContainer(pauseContainerAction);
    }

    @MessageMapping("/container/unpause")
    public void unpauseContainer(final UnpauseContainerAction unpauseContainerAction) throws DockerException, InterruptedException {
        containerService.unpauseContainer(unpauseContainerAction);
    }

    @MessageMapping("/container/start")
    public void startContainer(final StartContainerAction startContainerAction) throws DockerException, InterruptedException {
        containerService.startContainer(startContainerAction);
    }

    @MessageMapping("/container/kill")
    public void killContainer(final KillContainerAction killContainerAction) throws DockerException, InterruptedException {
        containerService.killContainer(killContainerAction);
    }

    @MessageMapping("/container/restart")
    public void restartContainer(final RestartContainerAction restartContainerAction) throws DockerException, InterruptedException {
        containerService.restartContainer(restartContainerAction);
    }

    @MessageExceptionHandler
    @SendToUser("/queue/container/error")
    public String handleException(final Throwable exception) {
        return exception.getMessage();
    }
}
