package robertograham.manifest.server.controller;

import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ImageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import robertograham.manifest.server.service.ImageService;

import java.util.Objects;

@Controller
public final class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(final ImageService imageService) {
        this.imageService = Objects.requireNonNull(imageService, "imageService cannot be null");
    }

    @MessageMapping("/image/{imageId}/info")
    @SendToUser("/queue/image/{imageId}")
    public ImageInfo getImageInfoByImageId(@DestinationVariable final String imageId) throws DockerException, InterruptedException {
        return imageService.getImageInfoByImageId(imageId);
    }

    @MessageExceptionHandler
    @SendToUser("/queue/image/error")
    public String handleException(final Throwable exception) {
        return exception.getMessage();
    }
}
