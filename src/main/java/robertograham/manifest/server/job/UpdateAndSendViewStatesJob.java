package robertograham.manifest.server.job;

import com.spotify.docker.client.exceptions.DockerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpSubscription;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import robertograham.manifest.server.service.ContainerService;
import robertograham.manifest.server.service.ViewStateService;

import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public final class UpdateAndSendViewStatesJob {

    private static final Pattern CONTAINER_STATISTICS_TOPIC_PATTERN = Pattern.compile("^/topic/container/([a-z0-9]+)/statistics$");
    private final ContainerService containerService;
    private final SimpUserRegistry simpUserRegistry;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public UpdateAndSendViewStatesJob(final ContainerService containerService,
                                      final SimpUserRegistry simpUserRegistry,
                                      final SimpMessagingTemplate simpMessagingTemplate) {
        this.containerService = Objects.requireNonNull(containerService, "containerService cannot be null");
        this.simpUserRegistry = Objects.requireNonNull(simpUserRegistry, "simpUserRegistry cannot be null");
        this.simpMessagingTemplate = Objects.requireNonNull(simpMessagingTemplate, "simpMessagingTemplate cannot be null");
    }

    @Scheduled(fixedDelay = 1000)
    public void updateAndSendViewStates() {
        updateAndSendViewState(containerService, "/topic/container");
        updateActiveContainerViewStatistics();
    }

    private void updateAndSendViewState(final ViewStateService viewStateService,
                                        final String destination) {
        if (viewStateService.updateViewState())
            simpMessagingTemplate.convertAndSend(destination, viewStateService.viewState());
    }

    private void updateActiveContainerViewStatistics() {
        simpUserRegistry.findSubscriptions((final var simpSubscription) -> simpSubscription.getDestination()
            .matches(CONTAINER_STATISTICS_TOPIC_PATTERN.pattern()))
            .stream()
            .map(SimpSubscription::getDestination)
            .collect(Collectors.toUnmodifiableSet())
            .forEach((final var containerStatisticsDestination) -> {
                try (final var scanner = new Scanner(containerStatisticsDestination)) {
                    scanner.findAll(CONTAINER_STATISTICS_TOPIC_PATTERN)
                        .filter(Objects::nonNull)
                        .filter((final var matchResult) -> matchResult.groupCount() == 1)
                        .map((final var matchResult) -> matchResult.group(1))
                        .findFirst()
                        .ifPresent((final var containerIdString) -> {
                            try {
                                simpMessagingTemplate.convertAndSend(containerStatisticsDestination, containerService.getStatisticsByContainerId(containerIdString));
                            } catch (final DockerException | InterruptedException exception) {
                                exception.printStackTrace();
                            }
                        });
                }
            });
    }
}
