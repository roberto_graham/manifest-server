package robertograham.manifest.server.service;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.Signal;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import robertograham.manifest.server.action.*;
import robertograham.manifest.server.viewstate.ContainerViewState;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.spotify.docker.client.DockerClient.ListContainersParam.allContainers;

@Service
public final class ContainerService implements ViewStateService {

    private final DockerClient dockerClient;
    private final ContainerViewState containerViewState;

    @Autowired
    public ContainerService(final DockerClient dockerClient,
                            final ContainerViewState containerViewState) {
        this.dockerClient = Objects.requireNonNull(dockerClient, "dockerClient cannot be null");
        this.containerViewState = Objects.requireNonNull(containerViewState, "containerViewState cannot be null");
    }

    @Override
    public boolean updateViewState() {
        final var allContainersList = findAllContainers();
        if (!allContainersList.equals(containerViewState.getContainers())) {
            containerViewState.setContainers(allContainersList);
            return true;
        }
        return false;
    }

    @Override
    public ContainerViewState viewState() {
        return new ContainerViewState(containerViewState.getContainers(), containerViewState.getKillSignals());
    }

    public ContainerStats getStatisticsByContainerId(final String containerId) throws DockerException, InterruptedException {
        Objects.requireNonNull(containerId, "containerId cannot be null");
        return dockerClient.stats(containerId);
    }

    public void stopContainer(final StopContainerAction stopContainerAction) throws DockerException, InterruptedException {
        Objects.requireNonNull(stopContainerAction, "stopContainerAction cannot be null");
        dockerClient.stopContainer(stopContainerAction.getContainerId(), stopContainerAction.getWaitSeconds());
    }

    public void killContainer(final KillContainerAction killContainerAction) throws DockerException, InterruptedException {
        Objects.requireNonNull(killContainerAction, "killContainerAction cannot be null");
        final var signalNameString = killContainerAction.getKillSignal();
        if (signalNameString.isBlank())
            dockerClient.killContainer(killContainerAction.getContainerId());
        else
            dockerClient.killContainer(killContainerAction.getContainerId(), Signal.valueOf(signalNameString));
    }

    public void pauseContainer(final PauseContainerAction pauseContainerAction) throws DockerException, InterruptedException {
        Objects.requireNonNull(pauseContainerAction, "pauseContainerAction cannot be null");
        dockerClient.pauseContainer(pauseContainerAction.getContainerId());
    }

    public void unpauseContainer(final UnpauseContainerAction unpauseContainerAction) throws DockerException, InterruptedException {
        Objects.requireNonNull(unpauseContainerAction, "unpauseContainerAction cannot be null");
        dockerClient.unpauseContainer(unpauseContainerAction.getContainerId());
    }

    public void startContainer(final StartContainerAction startContainerAction) throws DockerException, InterruptedException {
        Objects.requireNonNull(startContainerAction, "startContainerAction cannot be null");
        dockerClient.startContainer(startContainerAction.getContainerId());
    }

    public void restartContainer(final RestartContainerAction restartContainerAction) throws DockerException, InterruptedException {
        Objects.requireNonNull(restartContainerAction, "restartContainerAction cannot be null");
        dockerClient.restartContainer(restartContainerAction.getContainerId(), restartContainerAction.getWaitSeconds());
    }

    private List<Container> findAllContainers() {
        try {
            return Optional.ofNullable(dockerClient.listContainers(allContainers()))
                .orElseGet(containerViewState::getContainers);
        } catch (final DockerException | InterruptedException ignored) {
            return containerViewState.getContainers();
        }
    }
}
