package robertograham.manifest.server.service;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ImageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public final class ImageService {

    private final DockerClient dockerClient;

    @Autowired
    public ImageService(final DockerClient dockerClient) {
        this.dockerClient = Objects.requireNonNull(dockerClient, "dockerClient cannot be null");
    }

    public ImageInfo getImageInfoByImageId(final String imageId) throws DockerException, InterruptedException {
        Objects.requireNonNull(imageId, "imageId cannot be null");
        return dockerClient.inspectImage(imageId);
    }
}
