package robertograham.manifest.server.service;

import robertograham.manifest.server.viewstate.ViewState;

public interface ViewStateService {

    boolean updateViewState();

    ViewState viewState();
}
