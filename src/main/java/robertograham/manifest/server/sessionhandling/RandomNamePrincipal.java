package robertograham.manifest.server.sessionhandling;

import java.security.Principal;
import java.util.Objects;
import java.util.UUID;

final class RandomNamePrincipal implements Principal {

    private final String name;

    RandomNamePrincipal() {
        name = UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        return "RandomNamePrincipal{" +
            "name='" + name + '\'' +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof RandomNamePrincipal))
            return false;
        final var randomNamePrincipal = (RandomNamePrincipal) object;
        return name.equals(randomNamePrincipal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String getName() {
        return name;
    }
}
