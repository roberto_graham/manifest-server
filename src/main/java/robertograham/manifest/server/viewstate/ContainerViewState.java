package robertograham.manifest.server.viewstate;

import com.spotify.docker.client.messages.Container;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public final class ContainerViewState implements ViewState {

    private List<Container> containers;
    private Set<String> killSignals;

    public ContainerViewState(final List<Container> containers, final Set<String> killSignals) {
        this.containers = List.copyOf(Objects.requireNonNull(containers, "containers cannot be null"));
        this.killSignals = Set.copyOf(Objects.requireNonNull(killSignals, "killSignals cannot be null"));
    }

    public List<Container> getContainers() {
        return containers;
    }

    public void setContainers(final List<Container> containers) {
        this.containers = List.copyOf(Objects.requireNonNull(containers, "containers cannot be null"));
    }

    public Set<String> getKillSignals() {
        return killSignals;
    }

    public void setKillSignals(final Set<String> killSignals) {
        this.killSignals = Set.copyOf(Objects.requireNonNull(killSignals, "killSignals cannot be null"));
    }

    @Override
    public String toString() {
        return "ContainerViewState{" +
            "containers=" + containers +
            ", killSignals=" + killSignals +
            '}';
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object)
            return true;
        if (!(object instanceof ContainerViewState))
            return false;
        final var containerViewState = (ContainerViewState) object;
        return containers.equals(containerViewState.containers) &&
            killSignals.equals(containerViewState.killSignals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(containers, killSignals);
    }
}
